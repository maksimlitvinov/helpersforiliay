<?php
// Получаем TPL
$tpl = filter_var($_POST['tpl'], FILTER_SANITIZE_STRING);
// Собираем путь к файлу
$file = __DIR__ . '/tpls/' . $tpl . '.tpl';
// Определяем ответ
$response = [
    'html' => '',
    'tpl' => $tpl,
    'success' => false
];
// Если такой файл существует, то
if (file_exists($file)) {
    // Говорим что все успешно
    $response['success'] = true;
    // Получаем контент файла
    $response['html'] = file_get_contents($file);
}
// И возвращаем ответ
exit(json_encode($response,1));