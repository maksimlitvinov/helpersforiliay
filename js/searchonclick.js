var uniSearch = {
    sel: {
        btn_search: 'a[data-tpl]',
        block_search: '.uni_search'
    },
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;

        $(this.sel.btn_search).on('click', function (e) {
            e.preventDefault();

            self.send({
                'tpl' : $(this).data('tpl')
            });
        });
    },
    send: function (data) {
        $.ajax({
            data: data,
            dataType: 'json',
            type: 'POST',
            url: '/scripts/getTpl.php',
            cache: false,
            success: function (data) {
                // Если успех
                if (data.success) {
                    // Выбираем соответственный блок и вставляем туда данные из tpl
                    $(uniSearch.sel.block_search).html(data.html);
                }
            }
        });
    }
};

$(document).ready(function() {
    uniSearch.init();
});