// Дожидаемся загрузки DOM
window.onload = function() {

    // Получаем URL
    var alias = window.location.pathname.split('/');

    // Получаем все ссылки, где utm_source=1
    var links = document.querySelectorAll('a[href*="utm_source=1"]');

    // Получаем нужный алиас
    alias = alias[alias.length - 1];

    // Проходимся по полученной коллекции
    for(var link of links) {

        // Получаем href ссылки
        var attr = link.href;

        // Устанавливаем новый href
        link.setAttribute('href', attr + '&utm_medium=' + alias);
    }
};