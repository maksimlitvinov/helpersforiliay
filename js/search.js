$(document).ready(function () {

    // Получаем юлоки в которые нужно подтянуть фигулину
    var tpls = $('*[data-tpl]');

    // Если такие блоки есть, то работает
    if (tpls.length > 0) {
        // Проходимся по каждому блоку
        tpls.each(function () {
            // Получаем tpl из дата-атрибута
            var tpl = $(this).data('tpl');
            // Отправляем запрос на сервер передавая tpl
            $.ajax({
                data: {tpl : tpl},
                dataType: 'json',
                type: 'POST',
                url: '/scripts/getTpl.php',
                cache: false,
                success: function (data) {
                    // Если успех
                    if (data.success) {
                        // Выбираем соответственный блок и вставляем туда данные из tpl
                        $('*[data-tpl='+data.tpl+']').html(data.html);
                    }
                }
            });
        });
    }
});